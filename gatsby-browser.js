const { notifyUpdate } = require('./src/ui/updating-snackbar');

exports.onServiceWorkerUpdateFound = notifyUpdate;
exports.onServiceWorkerActive = () => {
   window.location.reload();
};
