module.exports = {
   pathPrefix: '/mintgame',
   plugins: [
      'gatsby-plugin-typescript',
      'gatsby-plugin-react-helmet',
      'gatsby-plugin-material-ui',
      {
         resolve: 'gatsby-plugin-sentry',
         options: {
            dsn: process.env.SENTRY_DSN
         }
      },
      {
         resolve: 'gatsby-plugin-manifest',
         options: {
            cache_busting_mode: 'none',
            icon: 'icon.svg',
            name: 'Mint Game',
            short_name: 'Mint Game',
            display: 'standalone',
            start_url: '.',
            theme_color: '#932020',
            background_color: '#303030'
         }
      },
      {
         resolve: 'gatsby-plugin-offline',
         options: {
            workboxConfig: {
               globPatterns: ['**/*']
            }
         }
      },
      'gatsby-plugin-zopfli'
   ]
}
