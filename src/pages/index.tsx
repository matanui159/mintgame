import React from 'react';
import { Layout } from '../ui/layout';
import { Paper, Typography, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
   paper: {
      margin: theme.spacing(1),
      padding: theme.spacing(1),
      height: '50vh',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
   }
}));

export default function(): React.ReactElement {
   const classes = useStyles();

   return <Layout>
      {[...Array(10)].map((_, i) => 
         <Paper className={classes.paper} key={i}>
            <Typography variant='h6'>{i}</Typography>
         </Paper>
      )}
   </Layout>;
}
