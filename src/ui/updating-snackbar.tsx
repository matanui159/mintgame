import React from 'react';
import { Snackbar, SnackbarContent, makeStyles, Typography } from '@material-ui/core';
import { Update } from '@material-ui/icons';

let globalOpen = false;
let globalSetOpen = (open: boolean) => {
   globalOpen = open;
};

const useStyles = makeStyles(theme => ({
   content: {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.secondary.contrastText
   },
   message: {
      display: 'flex',
      alignItems: 'center'
   },
   icon: {
      marginRight: theme.spacing(1)
   }
}));

export function UpdatingSnackbar(): React.ReactElement {
   const [open, setOpen] = React.useState(globalOpen);
   globalSetOpen = setOpen;
   const classes = useStyles();

   return <Snackbar
      anchorOrigin={{
         vertical: 'bottom',
         horizontal: 'left'
      }}
      open={open}
   >
      <SnackbarContent
         className={classes.content}
         message={<Typography variant='subtitle1' className={classes.message} >
            <Update className={classes.icon} />
            Updating...
         </Typography>}
      />
   </Snackbar>
}

export function notifyUpdate(): void {
   globalSetOpen(true);
}
