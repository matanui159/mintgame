import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import 'typeface-roboto';
import { createMuiTheme, AppBar, Toolbar, Typography, IconButton, makeStyles, MuiThemeProvider } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import { UpdatingSnackbar } from './updating-snackbar';
import Helmet from 'react-helmet';
import { red, blue } from '@material-ui/core/colors';

const theme = createMuiTheme({
   palette: {
      type: 'dark',
      primary: {
         main: red[700]
      },
      secondary: {
         main: blue.A700
      }
   }
});

interface LayoutProps {
   children: React.ReactNode;
}

export function Layout(props: LayoutProps): React.ReactElement {
   return <MuiThemeProvider theme={theme}>
      <Helmet>
         <html lang='en' />
         <title>Mint Game</title>
         <meta name='description' content='Mobile optimised PWA game engine' />
      </Helmet>
      <CssBaseline />
      <UpdatingSnackbar />
      <AppBar position='fixed'>
         <Toolbar>
            <IconButton color='inherit' edge='start'>
               <ArrowBack />
            </IconButton>
            <Typography variant='h6'>Mint Game</Typography>
         </Toolbar>
      </AppBar>
      <Toolbar />
      {props.children}
   </MuiThemeProvider>
}
